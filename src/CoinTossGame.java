/**
 * Created by Dmitry Vereykin on 12/15/2015.
 */
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

//Regular frame, nothing interesting
public class CoinTossGame extends JFrame {
    private JButton run;
    private JButton clear;
    private JPanel panelButton;
    private JPanel panelShow;
    private JTextArea textArea;
    private JScrollPane scrollPane;

    public CoinTossGame() {
        this.setTitle("Coin Toss Game");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        run = new JButton("    Run    ");
        clear = new JButton("    Clear    ");

        textArea = new JTextArea(40, 50);
        scrollPane = new JScrollPane(textArea);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        textArea.setLineWrap(true);

        run.addActionListener(new ButtonListener());
        clear.addActionListener(new ButtonListener());

        panelButton = new JPanel();
        panelButton.add(run);
        panelButton.add(clear);

        panelShow = new JPanel();
        panelShow.add(scrollPane);


        this.add(panelButton, BorderLayout.SOUTH);
        this.add(panelShow, BorderLayout.NORTH);
        this.pack();

        setSize(600, 730);
        setVisible(true);
        setLocationRelativeTo(null);
    }

    //Unfortunately, I didn't have time to write runnable (maybe later)
    private class ButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == run) {
                Trainer trainer = new Trainer();
                trainer.play();
                textArea.setText(trainer.getStr());
            } else if (e.getSource() == clear)
                textArea.setText(null);
        }
    }

    public static void main(String[] args) {
        new CoinTossGame();
    }

}