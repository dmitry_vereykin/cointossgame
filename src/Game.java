import java.util.ArrayList;

public class Game {
    public ArrayList<Move> moves = new ArrayList<>();
    public int movesNumber  = 0;

    //I moved part of the code from Trainer, particularly this one does initial move HTH to something
    public Move initialMove() {
        Move temporary = new Move("H", "T", "H");
        int random = random(0, 2);
        temporary.flip(random);
        setLegality(random, temporary);
        return temporary;
    }

    //This method grabs previous move (specifically final state) and flips a coin
    //Additionally, it check whether previous one was legal or not, and if it wasn't, it call initialMove
    public Move move(Move previousMove) {
        Move temporary;
        if (previousMove.getLegalMove()) {
            temporary = new Move(previousMove.getFinalState());
            int random = random(0, 2);
            temporary.flip(random);
            setLegality(random, temporary);
        } else {
            temporary = initialMove();
        }
        return temporary;
    }

    //Next two methods check legality and store the value as a boolean variable inside Move object
    public void setLegality(int random, Move comparingMove) {
        if (legalMove(random, comparingMove))
            comparingMove.setLegalMove(true);
        else
            comparingMove.setLegalMove(false);
    }


    public boolean legalMove(int index, Move comparingMove) {
        if (index == 1)
            return true;
        else if (index == 0 && comparingMove.finalState.getCoin(1).equals(comparingMove.finalState.getCoin(2)))
            return true;
        else if (index == 2 && comparingMove.finalState.getCoin(0).equals(comparingMove.finalState.getCoin(1)))
            return true;
        else
            return false;
    }

    //Checks winning state: the final state have to be THT and a move have to be legal
    public boolean checkWin(Move comparingMove) {
        movesNumber++;
        if (comparingMove.getLegalMove() && comparingMove.finalState.getCoin(0).equals("T") && comparingMove.finalState.getCoin(1).equals("H") && comparingMove.finalState.getCoin(2).equals("T"))
            return true;
        else
            return false;
    }

    public int getMovesNumber() {
        return movesNumber;
    }

    //Separate method for generating random number (just for convenience)
    public int random(int min, int max) {
        int range = (max - min) + 1;
        return (int)(Math.random() * range) + min;
    }

}
